const express = require("express");
const calc = require('./calc');

var app = express();

app.get('/', (req, res) => {
	res.end('Hello World!');
});


app.get('/adition/:n1/:n2', (req, res) => {
	let { n1, n2 } = req.params;

	let result = calc.adition(n1, n2);
	res.end(result);
});

app.get('/subtraction/:n1/:n2', (req, res) => {
	let { n1, n2 } = req.params;

	let result = calc.subtraction(n1, n2);
	res.end(result);
});

app.get('/multiplication/:n1/:n2', (req, res) => {
	let { n1, n2 } = req.params;

	let result = calc.multiplication(n1, n2);
	res.end(result);
});

app.get('/division/:n1/:n2', (req, res) => {
	let { n1, n2 } = req.params;

	let result = calc.division(n1, n2);
	res.end(result);
});

app.listen(5000, () => {
	console.log('running at port 5000');
});
