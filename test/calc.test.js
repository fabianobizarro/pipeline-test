const assert = require('assert');
const calc = require('../calc');

describe('Adition', () => {
    it('should add 2 numbers', () => {

        let n1 = 2, n2 = 2;
        assert.equal(calc.adition(n1, n2), 4);

    });
});

describe('Sutraction', () => {
    it('should subtract 2 numbers', () => {

        let n1 = 20, n2 = 10;
        assert.equal(calc.subtraction(n1, n2), 10);

    });
});

